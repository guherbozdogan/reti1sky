# reti1Sky


This component creates a webgl based sky renderer in which user could control the following parameters:

Camera's latitude/ longitude value
Camera's distance to center of earth
Datetime in Gregorgian Calendar

Creates sky shaders that work configurable times per n frames to draw perlin based 3d cloud textures/ color the sky in perspective far plane with using sun's position in sky with a slightly realistic physical rendering for webgl based games

The license is GPL since utilizes GPL based module's two methods.
The utilized GPL modules  (that's license is shared along is) that are utilized/taken ways from are as:

+ https://github.com/hebcal/ (For sun's location in sky)
+ https://github.com/cedricpinson/osgjs  (rendering engine)









